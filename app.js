/*
GAME RULES:

- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game

*/
var scores, currentScores, roundScore, activePlayer, dice2, targetScore, prevDices, dicesNumber;
//Dom vars
var currentScore0, currentScore1, domDices, btnRoll, domScores, btnHold, name0, name1, domNames, btnNewGame, playerPanels, domCurrentScores, domTargetScore;

dicesNumber = 2;
dices = new Array(dicesNumber).fill(0);
prevDices = new Array(dicesNumber).fill(0);
targetScore = 50;

btnRoll = document.getElementById('btn-roll-dice');
domDices = [];
for(let i = 0; i < dicesNumber; i++) {
    domDices.push(document.getElementById(`dice-img-${i}`));
}
currentScore0 = document.getElementById('current-0');
currentScore1 = document.getElementById('current-1');
score0 = document.getElementById('score-0');
score1 = document.getElementById('score-1');
name0 = document.getElementById('name-0');
name1 = document.getElementById('name-1');
btnHold = document.querySelector('.btn-hold');
btnNewGame = document.querySelector('.btn-new');
playerPanel0 = document.querySelector('.player-0-panel');
playerPanel1 = document.querySelector('.player-1-panel');
domTargetScore = document.getElementById('inpt-target-score');

domTargetScore.value = targetScore;
playerPanels = [playerPanel0, playerPanel1];
domCurrentScores = [currentScore0, currentScore1];
domScores = [score0, score1];
domNames = [name0, name1];

btnNewGame.addEventListener('click', onBtnNewGameClick);
function onBtnNewGameClick(event){
    removeEventListeners();
    activateNewGameState();
}

activateNewGameState();

function activateNewGameState() {
    scores = [0, 0];
    dices = new Array(dicesNumber).fill(0);
    prevDices = new Array(dicesNumber).fill(0);
    currentScores = [0, 0];
    roundScore = 0;
    activePlayer = 0;
    targetScore = domTargetScore.value;
    
    playerPanels[0].classList.remove('active');
    playerPanels[1].classList.remove('active');
    playerPanels[0].classList.remove('winner');
    playerPanels[1].classList.remove('winner');
    playerPanels[0].classList.add('active');

    domCurrentScores[0].textContent = currentScores[0];
    domCurrentScores[1].textContent = currentScores[1];

    domScores[0].textContent = scores[0];
    domScores[1].textContent = scores[1];

    domNames[0].textContent = 'Player 1';
    domNames[1].textContent = 'Player 2';
    
    btnHold.addEventListener('click', onBtnHoldClick);
    btnRoll.addEventListener('click', onBtnRollClick);

    hideDices();
}

function activateWinnerState() {
    playerPanels[activePlayer].classList.remove('active');
    domNames[activePlayer].textContent = 'WINNER';
    playerPanels[activePlayer].classList.add('winner');
    removeEventListeners();
}

function hideDices() {
    for(let i = 0; i < domDices.length; i++) {
        domDices[i].style.display = 'none';
    }
}

function showDices() {
    for(let i = 0; i < domDices.length; i++) {
        domDices[i].style.display = 'block';
    }
}

btnHold.addEventListener('click', onBtnHoldClick);
function onBtnHoldClick(event) {
    saveActivePlayerScore();
    resetActivePlayerCurrentScore();
    hideDices();
    if(scores[activePlayer] >= targetScore) {
        activateWinnerState();
    } else {
        toggleActivePlayer();
    }
}

btnRoll.addEventListener('click', onBtnRollClick);
function onBtnRollClick(event){
    showDices();

    prevDices = dices;
    for(let i = 0; i < dices.length; i++){
        dices[i] = Math.ceil(Math.random() * 6);
    }
    console.log(`Dice: ${dices}`);
    updateDOMDiceScore(dices);
    if(dices.indexOf(1) != -1) {
        hideDices();
        resetActivePlayerCurrentScore();
        toggleActivePlayer();
    } else if(prevDices.every(function(v) { return v === 6; }) && dices.every(function(v) { return v === 6; })){
        hideDices();
        resetActivePlayerCurrentScore();
        resetActivePlayerAllScore();
        toggleActivePlayer();
    } else {
        updateActivePlayerCurrentScore(dices);
    }
}

function toggleActivePlayer() {
    activePlayer = activePlayer ? 0 : 1;
    togglePlayerPanels();
}

function updateDOMDiceScore(scores) {
    function getDiceImagePath(score){
        return `dice-${score}.png`;
    }
    for(let i = 0; i < domDices.length; i++) {
        domDices[i].src = getDiceImagePath(scores[i]);
    }
}

function saveActivePlayerScore() {
    scores[activePlayer] += currentScores[activePlayer]
    domScores[activePlayer].textContent = scores[activePlayer];
}

function updateActivePlayerCurrentScore(score) {
    currentScores[activePlayer] += score.reduce(function(accumulator, currentValue) {
        return accumulator + currentValue;
    }, 0);
    domCurrentScores[activePlayer].textContent = currentScores[activePlayer];
}

function resetActivePlayerCurrentScore() {
    currentScores[activePlayer] = 0;
    domCurrentScores[activePlayer].textContent = 0;
}

function resetActivePlayerAllScore() {
    scores[activePlayer] = 0;
    domScores[activePlayer].textContent = 0;
}

function removeEventListeners() {
    btnHold.removeEventListener('click', onBtnHoldClick);
    btnRoll.removeEventListener('click', onBtnRollClick);
}

function togglePlayerPanels() {
    playerPanels[0].classList.toggle('active');
    playerPanels[1].classList.toggle('active');
}


